import gulp from 'gulp';
import { create } from 'browser-sync';

export const browserSync = create();

gulp.task('serve', ['sass:watch'], () => {

  browserSync.init({
    server: "./"
  });

});
