import gulp from "gulp";
import runSequence from 'run-sequence';

gulp.task('default', (done) => {
  return runSequence('clean', 'serve', done);
});
