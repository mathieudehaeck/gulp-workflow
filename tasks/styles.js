import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import autoprefixer from "autoprefixer";
import cssnano from "cssnano";
import { browserSync } from "./serve";

const plugins = gulpLoadPlugins();

gulp.task('sass', () => {
  const postCssPlugins = [
    autoprefixer({browsers: ['last 1 version']}),
    cssnano()
  ];
  return gulp.src(['./src/css/**/*.scss'])
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass().on('error', plugins.sass.logError))
      .pipe(plugins.postcss(postCssPlugins))
      .pipe(plugins.sourcemaps.write())
      .pipe(gulp.dest('.tmp/'))
      .pipe(browserSync.stream());
});
