import gulp from 'gulp';

gulp.task('sass:watch', ['sass'], () => {
  gulp.watch('./src/css/**/*.scss', ['sass']);
});
