import walkSync from 'walk-sync';

walkSync.entries('./tasks', {directories: false, globs: ['**/*.js']})
    .map((fileEntry) => {
      return fileEntry.basePath + "/" + fileEntry.relativePath;
    })
    .map(require);
